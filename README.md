# apt-transport-tor

Easily install Debian packages via [Tor](https://www.torproject.org).

This package implements an APT "acquire method" that handles URLs starting
with "tor+http://" or "tor+https://" in your sources.list.

## Usage

Edit your /etc/apt/sources.list like so, adjusting the suite/components
appropriately for your system:

    deb     tor+http://deb.debian.org/debian unstable main
    deb-src tor+http://deb.debian.org/debian unstable main

Note that [deb.debian.org](https://deb.debian.org/) is backed by a set of
commercial [CDN](https://en.wikipedia.org/wiki/Content_delivery_network)s,
so that you will likely be served by one of their servers close (in network
terms) to the exit node of your connection automatically.

You can of course use any other Debian mirror from the extensive [mirror
list](https://debian.org/mirror/list), but that might result in a connection
"across the globe" if the chosen exit node is far away from the mirror causing
slower connection speeds.

Alternatively, if you have the Tor onion service address of a Debian
mirror, you can use that to have your traffic never leave the Tor network:

    deb     tor+http://<long string>.onion/debian unstable main
    deb-src tor+http://<long string>.onion/debian unstable main

## APT repositories as onion services

While apt sends no directly identifying information to mirrors the download of
metadata like the Translation files as well as individual package names can
potentially reveal information about a user an adversary could observe.
If this is a concern, some APT repositories are available as onion services
which means the information doesn't leave the Tor network via an exit node:

Debian Project: [Complete List](https://onion.debian.org) [Announcement](https://bits.debian.org/2016/08/debian-and-tor-services-available-as-onion-services.html)

 * ftp.debian.org: tor+http://vwakviie2ienjx6t.onion/
 * security.debian.org: tor+http://sgvtcaew4bxjd7ln.onion/
 * people.debian.org: tor+http://hd37oiauf5uoz7gg.onion/
 * debug.mirrors.debian.org: tor+http://ktqxbqrhg5ai2c7f.onion/
 * incoming.debian.org: tor+http://oscbw3h7wrfxqi4m.onion/
 * ftp.ports.debian.org: tor+http://nbybwh4atabu6xq3.onion/
 * incoming.ports.debian.org: tor+http://vyrxto4jsgoxvilf.onion/

Tor Project: [Complete List](https://onion.torproject.org/) [Announcement](https://blog.torproject.org/blog/debian-and-tor-services-available-onion-services)

 * deb.torproject.org: tor+http://sdscoq7snqtznauu.onion/

Note that this list might not be current: Verify before use! The list is
provided only to showcase that many commonly used repositories are already
available as an onion service.

## Configuration

### Preventing user identification by languages

APT sents no directly user identifying data to a server, but the server (and
any observer between you and the server) can guess based on the languages apt
downloads data for which languages the user might speak and from that infere
culture and/or origin country of the user. With a particular uncommon set it
might even be possible to identify a user.

The most obvious solution might be to configure apt to not download data for
any language (or only for english) via the Acquire::Languages option, but this
is unacceptable if e.g. some or all users do not understand english. The option
can also be used to add or remove certain languages to the list. The download
of Translation files (which include the long descriptions for packages) can also
be configured for individual sources via sources.list options.

### Using a different Tor instance

By default, apt-transport-tor uses the following SOCKS proxy setting, which
is the default location of a locally installed Tor instance:

	Acquire::tor::proxy "socks5h://apt-transport-tor@localhost:9050";

Note the use of a username to make use of the default IsolateSOCKSAuth Tor
setting for stream isolation, which requires Tor 0.2.4.19 to work well.
This means your apt traffic will be sent over a different circuit from your
regular Tor traffic and for each host you connect to.

### Disabling use of http(s) without Tor in APT

APT >= 1.3 allows methods to be disabled without removing them from the system,
so to avoid mistakenly adding new sources without using tor you can tell apt
via the following configuration options to fail for non-tor-http(s) sources:

	Dir::Bin::Methods::http "false";
	Dir::Bin::Methods::https "false";

### Downloading changelogs

The locations of changelogs is independent of repository. The Release file can
and should include the URI changelogs can be found on, which tends to be an http
URI of a central service.

You can override the value from the Release file to use Tor here as well, or if
you happen to know an onion address use this one instead. the following listing
gives three valid configurations for Debian where the first one is the default,
the second uses the default via Tor and the third uses an onion service address.

	Acquire::Changelogs::URI::Override::Origin::Debian "http://metadata.ftp-master.debian.org/changelogs/@CHANGEPATH@_changelog";
	Acquire::Changelogs::URI::Override::Origin::Debian "tor+http://metadata.ftp-master.debian.org/changelogs/@CHANGEPATH@_changelog";
	Acquire::Changelogs::URI::Override::Origin::Debian "tor+http://cmgvqnxjoiqthvrc.onion/changelogs/@CHANGEPATH@_changelog";

### Using apt-transport-mirror together with Tor

apt >= 1.6 comes with a renewed mirror method which can also work correctly
with Tor: Any mirror file can be used for this by prefixing the access method
used to acquire the mirror file with `tor+`. If the mirror file is on your
local disk it should hence be `tor+mirror+file:/path/to/mirror.file` while if
it is accessed via HTTP it will look like `tor+mirror+http://example.org/path/to/mirror.file`.

This will automatically prefix each access method mentioned in the mirror file
with `tor+`. Note that you can't specify e.g. `tor+http` in the mirror file
directly to prevent the server to detect if the client has Tor support. If you
want to include an onion service in the mirror file just use `http` as access
method for it. A client without support for Tor accessing this mirror file will
fall back to the next mirror just fine.

## Caveats

Downloading your Debian packages over Tor prevents an attacker who is
sniffing your network connection from being able to tell which packages
you are fetching, or even that your traffic is Debian-related.

However, this does not necessarily defend you from, amongst other things:

* a global passive adversary (who could potentially correlate the exit
  node's traffic with your local Tor traffic)
* an attacker looking at the size of your downloads, and making an
  educated guess about the contents
* an attacker who has broken into your machine

Download speeds will be slower via Tor.

## Trademark, Copyright & Licensing

This product is produced independently from the Tor® anonymity software and
carries no guarantee from [The Tor Project](https://www.torproject.org) about
quality, suitability or anything else.

    Copyright (C) 2014 Tim Retout <diocles@debian.org>
    Copyright (C) 2016-2018 David Kalnischkies <donkult@debian.org>

License:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

## Feedback

Comments and suggestions to: [APT developer mailinglist](mailto:deity@lists.debian.org) ([archive](https://lists.debian.org/deity/))

Bug reports should be sent to the Debian BTS.
